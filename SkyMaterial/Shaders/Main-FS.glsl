#version 430
#extension GL_ARB_bindless_texture : enable
#extension GL_ARB_shading_language_include : enable
#include "/Materials/Common/Common"
#line 6 



 layout(std140) uniform CPU
{	
	vec4 CPU_color;
	sampler2D maTexture;
}; 

layout (location = 0) out vec4 Color;

in vec3 position_objet; 
in vec3 normal; 


layout(location = 3) in vec3 v_TexCoord;
in vec3 coor;

void main()
{	


	vec4 TColor = texture(maTexture, coor.xy); 

	Color = TColor;

}