#include "SkyMaterial.h"
#include "Engine/Base/Node.h"
#include "Engine/Base/Scene.h"



SkyMaterial::SkyMaterial(std::string name, glm::vec4 & c):
	MaterialGL(name,"SkyMaterial")
{
	GPUsampler* tmp;
	
	modelViewProj = vp->uniforms()->getGPUmat4("MVP");
	temps = vp->uniforms()->getGPUfloat("t");
	
	poslum = vp->uniforms()->getGPUvec3("lum_pos");  //position de la lumi�re donn� au GPU
	coeff = vp->uniforms()->getGPUvec3("coeff");  //coefficient 
	color = fp->uniforms()->getGPUvec4("CPU_color");
	text = new GPUTexture2D("./Textures/sky3.jpg");
	tmp = fp->uniforms()->getGPUsampler("maTexture");
	tmp->Set(text->getHandle());
	color->Set(c);
}
SkyMaterial::~SkyMaterial()
{

}

void SkyMaterial::setColor(glm::vec4 & c)
{
	color->Set(c);
}

void SkyMaterial::render(Node *o)
{
	if (m_ProgramPipeline)
	{		
		m_ProgramPipeline->bind();
		o->drawGeometry(GL_TRIANGLES);
		m_ProgramPipeline->release();
	}
}

void SkyMaterial::update(Node* o,const int elapsedTime)
{
	temps-> Set(elapsedTime); //On lui assigne une valeur (qui sera t dans le GPU)
	modelViewProj->Set(o->frame()->getTransformMatrix());
	
}

void SkyMaterial::setPosition(glm::vec3 & poslum)
{
	poslum = glm::vec3(10.0, 20.0, 0.0);
}

void SkyMaterial::setCoeff(glm::vec3 & coeff) 
{
	coeff = glm::vec3(0.2, 0.6, 0.2);
}
