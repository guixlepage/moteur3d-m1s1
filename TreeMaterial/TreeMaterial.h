#ifndef _TREEMATERIAL_H
#define _TREEMATERIAL_H


#include "Engine/OpenGL/MaterialGL.h"
#include "Engine/OpenGL/Lighting/LightingModelGL.h"
#include <memory.h>

class TreeMaterial : public MaterialGL
{
public:
	TreeMaterial(std::string name, glm::vec4 & c = glm::vec4(0.5, 0.5, 0.5, 1.0));
	~TreeMaterial();
	void setColor(glm::vec4 & c);
	void setPosition(glm::vec3 & p);
	void setCoeff(glm::vec3 & coeff);

	virtual void render(Node *o);
	virtual void update(Node* o, const int elapsedTime);


private:
	GPUvec4* color;
	GPUmat4* modelViewProj;
	GPUfloat* temps; //permettra d'envoyer le temps au GPU
	GPUvec3* poslum; //position de la lumi�re
	GPUvec3* coeff; //coefficient 
	GPUTexture2D* text;

};

#endif