#include "MyColorMaterial.h"
#include "Engine/Base/Node.h"
#include "Engine/Base/Scene.h"


MyColorMaterial::MyColorMaterial(std::string name, glm::vec4 & c):
	MaterialGL(name,"MyColorMaterial")
{

	GPUsampler* sampler;
	
	modelViewProj = vp->uniforms()->getGPUmat4("MVP");
	color = fp->uniforms()->getGPUvec4("CPU_color");

	temps = vp->uniforms()->getGPUfloat("t");

	pos_cam = fp->uniforms()->getGPUvec3("pos_cam");
	color->Set(c);

	Sky = new GPUTexture2D("./Textures/sky3.jpg");
	sampler = fp->uniforms()->getGPUsampler("Sky");
	sampler->Set(Sky->getHandle());

	eau = new GPUTexture2D("./Textures/eau.jpg"); //n_water.jpg
	sampler = fp->uniforms()->getGPUsampler("eau");
	sampler->Set(eau->getHandle());


	normal = new GPUTexture2D("./Textures/normal.png"); //n_water.jpg
	sampler = vp->uniforms()->getGPUsampler("test");
	sampler->Set(normal->getHandle());


	/* dudv = new GPUTexture2D("./Textures/ok.jpg");
	sampler = vp->uniforms()->getGPUsampler("test");
	sampler->Set(dudv->getHandle()); */

}
MyColorMaterial::~MyColorMaterial()
{

}

void MyColorMaterial::setColor(glm::vec4 & c)
{
	color->Set(c);
}

void MyColorMaterial::render(Node *o)
{
	if (m_ProgramPipeline)
	{		
		m_ProgramPipeline->bind();
		o->drawGeometry(GL_TRIANGLES);
		m_ProgramPipeline->release();
	}
}

void MyColorMaterial::update(Node* o,const int elapsedTime)
{
	t += elapsedTime;
	temps->Set(t); //On lui assigne une valeur (qui sera t dans le GPU)
	modelViewProj->Set(o->frame()->getTransformMatrix());
	
}

void MyColorMaterial::setCamera(glm::vec3 & pos_camera)
{
	pos_cam->Set(pos_camera);
}