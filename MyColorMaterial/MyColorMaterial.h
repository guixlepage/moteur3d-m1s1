#ifndef _COLORMATERIAL_H
#define _COLORMATERIAL_H


#include "Engine/OpenGL/MaterialGL.h"
#include "Engine/OpenGL/Lighting/LightingModelGL.h"
#include <memory.h>

class MyColorMaterial : public MaterialGL
{
	public:
		MyColorMaterial(std::string name, glm::vec4 & c = glm::vec4(0.5,0.5,0.5,1.0));
		~MyColorMaterial();
		void setColor(glm::vec4 & c);

		virtual void render(Node *o);
		virtual void update(Node* o,const int elapsedTime);
		void MyColorMaterial::setCamera(glm::vec3 & pos_camera);
		

	private:
		float t;
		GPUvec3* pos_cam;
		GPUvec4* color;
		GPUmat4* modelViewProj;
		GPUfloat* temps; //permettra d'envoyer le temps au GPU
		GPUvec3* camPos;
		GPUTexture2D* Sky;
		GPUTexture2D* normal;
		GPUTexture2D* eau;
		//GPUTexture2D* dudv;
};

#endif