#version 430
#extension GL_ARB_bindless_texture : enable
#extension GL_ARB_shading_language_include : enable
#include "/Materials/Common/Common"
#line 6 



layout(std140) uniform CPU
{	
	vec4 CPU_color;
	float coucou;
	vec3 pos_cam;
	sampler2D Sky;
	sampler2D eau;

};



layout (location = 0) out vec4 Color;

in vec3 position_objet;
in vec3 normal;
in vec3 coor;
in float time;

void main()
{	

	//VARIABLES 
	vec3 position_camera = pos_cam;

 /*-----------------------  REFLECT  ---------------*/ 
	
	vec2 offset = vec2(1.0,0.5)*(time/500)*0.05;
	vec4 water = texture2D(eau, (coor.xy + offset));

	vec3 tex1 = texture(Sky, reflect(normalize(pos_cam), normalize(normal)).xy).rgb;
	vec4 tex12 = vec4(tex1,1.0);

	vec4 bleu = vec4(0.0, 0.0, 0.8,0.8);
	Color =  (bleu*0.3) + (water*0.4) + (0.1*tex12) ;
	Color.a = 0.5; 

}