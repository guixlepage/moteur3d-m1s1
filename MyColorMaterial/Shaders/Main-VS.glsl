#version 430
#extension GL_ARB_bindless_texture : enable
#extension GL_ARB_shading_language_include : enable
#include "/Materials/Common/Common"
#line 6 


layout(std140) uniform CPU
{
	mat4 MVP;
	float t;
    sampler2D test;
	//sampler2D dudv;
	
};

 out gl_PerVertex {
        vec4 gl_Position;
        float gl_PointSize;
        float gl_ClipDistance[];
    };
layout (location = 0) in vec3 Position;
layout(location = 2) in vec3 Normal;
layout(location = 3) in vec3 TexCoord;


out vec3 position_objet;
out vec3 normal;
out vec3 coor;
out float time;


void main()
{
	time = t;
	position_objet = Position;


	normal = Normal;
	
	float sablier = (t*0.009);
	vec2 decalage = vec2(1.0,0.5)*sablier*0.05;

	float hauteur = texture2D(test, TexCoord.xy + decalage).r *0.009;
	float hauteur2 = texture2D(test, TexCoord.xy + decalage).b * 0.006;
	float hauteur3 = texture2D(test, TexCoord.xy + decalage).g * 0.003;

	normal = texture2D(test, TexCoord.xy).rbg;
	position_objet.y = position_objet.y + hauteur + hauteur2 + hauteur3;
	normal.y = normal.y + hauteur + hauteur2 + hauteur3; 


	coor = TexCoord;
	gl_Position = MVP * vec4(position_objet,0.1);

}
