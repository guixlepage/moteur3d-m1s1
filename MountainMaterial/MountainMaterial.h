#ifndef _MOUNTAINMATERIAL_H
#define _MOUNTAINMATERIAL_H


#include "Engine/OpenGL/MaterialGL.h"
#include "Engine/OpenGL/Lighting/LightingModelGL.h"
#include <memory.h>

class MountainMaterial : public MaterialGL
{
public:
	MountainMaterial(std::string name, glm::vec4 & c = glm::vec4(0.5, 0.5, 0.5, 1.0));
	~MountainMaterial();
	void setColor(glm::vec4 & c);
	void setPosition(glm::vec3 & p);
	void setCoeff(glm::vec3 & coeff);
	/*void setPosLum(glm::vec3 & pos_lum); */

	virtual void render(Node *o);
	virtual void update(Node* o, const int elapsedTime);


private:
	GPUvec4* color;
	GPUmat4* modelViewProj;
	GPUfloat* temps; //permettra d'envoyer le temps au GPU
	GPUvec3* poslum; //position de la lumi�re
	GPUvec3* coeff; //coefficient 
	GPUTexture2D* text;
	GPUTexture2D* NormalMap;

};

#endif