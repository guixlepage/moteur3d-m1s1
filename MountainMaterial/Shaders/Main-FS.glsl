#version 430
#extension GL_ARB_bindless_texture : enable
#extension GL_ARB_shading_language_include : enable
#include "/Materials/Common/Common"
#line 6 



 layout(std140) uniform CPU
{	
	vec4 CPU_color; //a mettre en com pour le changement de couleur par normale
	sampler2D maTexture;
	sampler2D NormalMap;
}; 

layout (location = 0) out vec4 Color;


in vec3 position_objet;
in vec3 normal;
in vec3 test; 

void main()
{	
	vec3 lumiere = vec3(10,10,10);
	vec3 position_camera = vec3(-5.61 , 10.0 , 25);
	vec3 V = normalize(position_objet - position_camera); //norme de vecteur de vue 
	vec3 N = normalize(normal);

	vec4 TColor = texture(maTexture, test.xy);

	//changement couleur par normale
	vec3 L = normalize(lumiere.xyz - V); 
	vec3 R = normalize(reflect(-L,N)); 
	vec3 E = normalize(position_camera - V); 

	vec3 Diffuse = TColor.xyz * max(dot(N, -L), 0.0);  
	vec3 Ambiant = TColor.xyz;
	vec3 Specular = TColor.xyz * pow(max(dot(R,normalize(V)),0.0),20);

	Color = vec4(0.2 * Ambiant + 0.5 * Diffuse + 0.2 * Specular, 1.0);

}