#include "MountainMaterial.h"
#include "Engine/Base/Node.h"
#include "Engine/Base/Scene.h"



MountainMaterial::MountainMaterial(std::string name, glm::vec4 & c):
	MaterialGL(name,"MountainMaterial")
{
	GPUsampler* tmp;
	
	modelViewProj = vp->uniforms()->getGPUmat4("MVP");
	temps = vp->uniforms()->getGPUfloat("t");
	
	poslum = vp->uniforms()->getGPUvec3("lum_pos");  //position de la lumi�re donn� au GPU
	coeff = vp->uniforms()->getGPUvec3("coeff");  //coefficient 
	color = fp->uniforms()->getGPUvec4("CPU_color");

	text = new GPUTexture2D("./Textures/rock.jpg"); //rock.JPG
	tmp = fp->uniforms()->getGPUsampler("maTexture");
	tmp->Set(text->getHandle());

	NormalMap= new GPUTexture2D("./Textures/rock_norm.JPG");
	tmp = fp->uniforms()->getGPUsampler("NormalMap");
	tmp->Set(text->getHandle());

	color->Set(c);
}
MountainMaterial::~MountainMaterial()
{

}

void MountainMaterial::setColor(glm::vec4 & c)
{
	color->Set(c);
}

void MountainMaterial::render(Node *o)
{
	if (m_ProgramPipeline)
	{		
		m_ProgramPipeline->bind();
		o->drawGeometry(GL_TRIANGLES);
		m_ProgramPipeline->release();
	}
}

void MountainMaterial::update(Node* o,const int elapsedTime)
{
	temps-> Set(elapsedTime); //On lui assigne une valeur (qui sera t dans le GPU)
	modelViewProj->Set(o->frame()->getTransformMatrix());
	
}

void MountainMaterial::setPosition(glm::vec3 & poslum)
{
	poslum = glm::vec3(5, 5, -15);  //(6.5, -1.5, -1810)
}

void MountainMaterial::setCoeff(glm::vec3 & coeff) 
{
	coeff = glm::vec3(0.2, 0.6, 0.2);
}

/* void MountainMaterial::setPosLum(glm::vec3 & pos_lum)
{
	poslum->Set(pos_lum);
} */