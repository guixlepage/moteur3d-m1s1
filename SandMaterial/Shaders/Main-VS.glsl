#version 430
#extension GL_ARB_bindless_texture : enable
#extension GL_ARB_shading_language_include : enable
#include "/Materials/Common/Common"
#line 6 

layout(location = 0) in vec3 Position;
layout(location = 2) in vec3 Normal;
layout(location = 3) in vec3 TexCoord;

layout(std140) uniform CPU   //on r�cup�re du CPU 
{
	mat4 MVP;
	float t; 
	vec3 lum_pos; 
	vec3 coeff;  
	
};

 out gl_PerVertex {
        vec4 gl_Position;
        float gl_PointSize;
        float gl_ClipDistance[];
    };


out vec3 position_objet;
out vec3 normal;
out vec3 coor; 

void main()
{

	position_objet = Position;
	normal = Normal;
	coor = TexCoord;
	gl_Position = MVP * vec4(position_objet,0.1);


}
