#version 430
#extension GL_ARB_bindless_texture : enable
#extension GL_ARB_shading_language_include : enable
#include "/Materials/Common/Common"
#line 6 



 layout(std140) uniform CPU
{	
	vec4 CPU_color;
	sampler2D maTexture;
}; 

layout (location = 0) out vec4 Color;

in vec3 position_objet; 
in vec3 normal; 


layout(location = 3) in vec3 v_TexCoord;
in vec3 coor;

void main()
{	
	vec3 var_lum_pos = vec3(10,10,10);
	vec3 position_camera = vec3(0.0,0.0,-10);
	vec3 V = normalize(position_objet - position_camera); //norme de vecteur de vue 
	vec3 N = normalize(normal);

	vec4 TColor = texture(maTexture, coor.xy); 

	
	/* ---------------------------- PHONG -------------------------------- */ 
	//changement couleur par normale
	vec3 L = -normalize(var_lum_pos - V); 
	vec3 R = reflect(L,N); 
	vec3 E = normalize(-position_camera); 

	vec3 DiffuseColor = TColor.xyz * max(dot(N,L),0.0); 
	vec3 AmbiantColor = TColor.xyz;
	vec3 SpecularColor = TColor.xyz * pow(max(dot(R,normalize(V)),0.0),20); 

	Color = vec4(0.6 * AmbiantColor + 0.6 * DiffuseColor + 0.6 * SpecularColor, 1.0);

	

}